# This file is a template, and might need editing before it works on your project.
FROM ruby:2.7-alpine

# Edit with nodejs, mysql-client, postgresql-client, sqlite3, etc. for your needs.
# Or delete entirely if not needed.
#RUN apk --no-cache add nodejs postgresql-client tzdata
RUN apk --no-cache add tzdata

# throw errors if Gemfile has been modified since Gemfile.lock
RUN bundle config --global frozen 1

WORKDIR /usr/src/app

COPY Gemfile Gemfile.lock .
# Install build dependencies - required for gems with native dependencies
#RUN apk add --no-cache --virtual build-deps build-base postgresql-dev && \
RUN apk add --no-cache --virtual build-deps build-base && \
  bundle install && \
  apk del build-deps

COPY . .

# For Sinatra
#EXPOSE 4567
#CMD ["ruby", "./config.rb"]

# For Rails
#ENV PORT 3000
#EXPOSE 3000
#CMD ["bundle", "exec", "rails", "server"]
